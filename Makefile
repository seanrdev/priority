.PHONY: all clean install install-strip

CXX ?= c++
INSTALL ?= install
PREFIX ?= /usr/local
BINDIR ?= bin

all: main a.out
install: main a.out
	$(INSTALL) $^ $(DESTDIR)/$(PREFIX)/$(BINDIR)/
install-strip: main a.out
	$(INSTALL) -s $^ $(DESTDIR)/$(PREFIX)/$(BINDIR)/
main: main.cpp PQueue.cpp Person.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
a.out: test.cpp PQueue.cpp Person.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS) -lsimon

PQueue.cpp: PQueue.h
Person.cpp: Person.h

clean:
	rm -f main a.out
