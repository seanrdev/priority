//
// Created by seanrdev on 9/29/17.
//

#include "PQueue.h"
#include "Person.h"

//Complete
PQueue::PQueue(void) {
    this->index = 0;
}

//Complete
bool PQueue::empty(void) {
    if(this->index == 0){
        return true;
    }
    return false;
}

//Complete
int PQueue::size(void) {
    return this->index;
}

Person * PQueue::front(void) {
    return lead;
}

Person * PQueue::back(void) {
    Person *temp;
    *temp = *lead;
    while(temp->getNext() != NULL){
        temp = temp->getNext();
    }
    return temp;
}


void PQueue::enqueue(Person *p) {
    if(this->index == 0){
        this->lead = p;
        lead->setNext(NULL);
        this->rear = p;
        rear->setNext(NULL);
        lead->setPrev(NULL);
        rear->setPrev(NULL);
        this->index++;
    } else{
        Person *temp;
        temp = this->lead;
        while(temp->getNext() != NULL || this->index == 1){
            if(temp->getNext() == NULL && temp->getTicket() > p->getTicket()){
                p->setNext(temp);
                p->setPrev(NULL);
                temp->setPrev(p);
                temp->setNext(NULL);
                this->lead = p;
                this->rear = temp;
                this->index++;
                return;
            }
            if(temp->getNext()->getTicket() > p->getTicket() && temp->getTicket() <= p->getTicket()){
                p->setNext(temp->getNext());
                p->setPrev(temp);
                temp->setNext(p);
                p->getNext()->setPrev(p);
                this->index++;
                return;
            }
            if(temp->getTicket() > p->getTicket() && temp->getPrev() == NULL){
                p->setNext(temp);
                temp->setPrev(p);
                p->setPrev(NULL);
                this->lead = p;
                this->index++;
                return;
            }
            temp = temp->getNext();
        }
        temp->setNext(p);
        p->setNext(NULL);
        temp->setNext(p);
        this->rear = p;
        this->index++;
    }
}


void PQueue::dequeue(void) {
    if(this->index == 1){
        cout << "Admitting " << this->lead->getName() << ".." << endl;
        delete lead;
        index--;
        return;
    }
    cout << "Admitting " << this->lead->getName() << ".." << endl;
    this->lead = lead->getNext();
    delete this->lead->getPrev();
    lead->setPrev(NULL);
    this->index--;
}

