//
// Created by seanrdev on 9/29/17.
//

#ifndef PRIORITY_PQUEUE_H
#define PRIORITY_PQUEUE_H


#include "Person.h"
#include <iostream>


class PQueue{
private:
    int index;
    Person *lead; //first in.
    Person *rear;
public:
    PQueue(void);
    bool empty(void);
    int size(void);
    Person * front(void);
    Person * back(void);
    void enqueue(Person *p);
    void dequeue(void);


};


#endif //PRIORITY_PQUEUE_H
