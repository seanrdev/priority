//
// Created by seanrdev on 9/29/17.
//

#include "Person.h"


Person::Person() {
    this->prev = NULL;
    this->next = NULL;
}

Person::~Person() {}

Person::Person(string name, int ticket){
    this->name = name;
    this->ticket = ticket;
}

string Person::getName() {
    return this->name;
}

int Person::getTicket() {
    return this->ticket;
}

Person * Person::getNext() {
    return this->next;
}

Person * Person::getPrev() {
    return this->prev;
}

void Person::setNext(Person *p) {
    this->next = p;
}

void Person::setPrev(Person *p) {
    this->prev = p;
}


