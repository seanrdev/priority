//
// Created by seanrdev on 9/29/17.
//

#ifndef PRIORITY_PERSON_H
#define PRIORITY_PERSON_H

#include <string>

using namespace std;

class Person{
private:
    Person *next;
    Person *prev;
    string name;
    int ticket;
public:
    Person();
	Person(string name, int ticket);
    ~Person();
    string getName();
    int getTicket();
    Person* getNext();
    Person* getPrev();
    void setNext(Person *p);
    void setPrev(Person *p);
};



#endif //PRIORITY_PERSON_H
