//
// Created by seanrdev on 10/4/17.
//

#include "Person.h"
#include "PQueue.h"
#include <fstream>
#include <regex>
#include <string>

int main(){
    ifstream my_file;
    my_file.open("/home/seanrdev/project_delete/input.txt");
    string input;
    PQueue *pq = new PQueue();
    while(getline(my_file, input)){
        [&](string s){
            regex rgx("([0-9]+) ([a-zA-Z]+)");
            smatch matches;
            if(regex_search(input, matches, rgx)){
                Person *per = new Person(matches[2].str(), stoi(matches[1].str()));
                pq->enqueue(per);
            }
        }(input);
    }
    while(pq->size() > 0){
        pq->dequeue();
    }
    return 0;
}