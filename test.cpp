#include <string>

#include "Person.h"
#include "PQueue.h"

#include <simon.h>

void random_string (char str[], size_t n) {
    size_t i;
    int tmp;
    for (i = 0; i != n; i++) {
        tmp = random_range_java ((int) CHAR_MIN, (int) CHAR_MAX);
        str[i] = (char) tmp;
    }
}

void random_string2 (char str[], size_t *n) {
    size_t i;
    int tmp;
    *n = random_range_java_size_t2 (1, *n);
    random_string (str, *n - 1);
    str[*n - 1] = '\0';
}

void random_Person (Person **restrict p) {
    char name[10];
    size_t n = ARRSZ (name);
    int ticket = random_range_java (-10, 10);
    random_string2 (name, &n);
    *p = new Person (name, ticket);
}

void random_Person2 (void *restrict p_) {
    Person **restrict p = (Person **restrict) p_;
    random_Person (p);
}

static bool isfull_cheap (void const *restrict unused) { return false; };
static void insert_cheap (void *restrict arg_, void const *restrict e) {
    PQueue *restrict arg = (PQueue *restrict) arg_;
    arg->enqueue ((Person *restrict) e);
}
static bool isempty_cheap (void const *restrict arg_) {
    PQueue *restrict arg = (PQueue *restrict) arg_;
    return arg->empty ();
}
static void remove_cheap (void *restrict arg_, void *restrict e_) {
    PQueue *restrict arg = (PQueue *restrict) arg_;
    Person **restrict e = (Person **restrict) e_;
    arg->dequeue ();
    TODO (you cannot see who gets dequeued?)
    *e = NULL;
}

__attribute__ ((nonnull (1), nothrow, warn_unused_result))
static int cheap_add_test (void *restrict arg_) {
   Person *tmp = new Person ();
   int err;
   err = add_test (arg_, &tmp,
      isfull_cheap, random_Person2, insert_cheap);
   if (err == TEST_NA) return 0;
   error_check (err != 0) return -1;
   return 0;
}

__attribute__ ((nonnull (1), nothrow, warn_unused_result))
static int cheap_remove_test (void *restrict arg_) {
   Person *tmp;
   int err;
   err = remove_test (arg_, &tmp,
      isempty_cheap, remove_cheap);
   if (err == TEST_NA) return 0;
   error_check (err != 0) return -1;
   return 0;
}


static int test_cb (void *restrict arg) {
   stdcb_t tests[2];

   TODO (more tests)
   tests[0] = cheap_add_test;
   tests[1] = cheap_remove_test;

   error_check (random_ops (arg, tests, ARRSZ (tests), 100) != 0) /* arbitrary params */
   /*random_ops2 (arg, tests, ARRSZ (tests));*/
      return -1;

   return 0;
}

int main (void){
    PQueue *restrict pq = new PQueue();
    test_cb (pq);
    while(! pq->empty()) pq->dequeue();
    return EXIT_SUCCESS;
}